rm(list = ls())
require('mailR')
errHandle = file('/home/admin/Logs/LogsGraphs.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

globCnt = 0
FORCESEND = 1
source('/home/admin/CODE/Send_mail/sendmail.R')
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'

while(1)
{
	recipients = getRecipients("Graphs","m")
	time = as.numeric(substr(as.character(format(Sys.time(),tz="Singapore")),12,13))
	if((time > 8 && time < 12 && globCnt > 15)|| FORCESEND)
	{
		today = substr(as.character(Sys.Date()),1,7)
		pathtoSend=c(
		"/home/admin/Jason/cec intern/results/KH008S/Rplots.pdf",
		"/home/admin/Jason/cec intern/results/[714]/Cambodia.pdf",
		"/home/admin/Jason/cec intern/results/[719]/Cebu.pdf",
		"/home/admin/Jason/cec intern/results/[720]/Penang.pdf",
		"/home/admin/Jason/cec intern/results/[714]/Ho_Chi_Minh.pdf",
		"/home/admin/Jason/cec intern/results/[718]/Karnataka.pdf",
		"/home/admin/Jason/cec intern/results/[711]/Delhi.pdf",
		"/home/admin/Jason/cec intern/results/[724]/Singapore.pdf",
		"/home/admin/Jason/cec intern/results/[713]/Chennai.pdf",
		"/home/admin/Jason/cec intern/results/[SG-006X]/Rplots.pdf",
		"/home/admin/Jason/cec intern/results/[724]/724_Graph.pdf",
		"/home/admin/Jason/cec intern/results/[725]/725_Graphs.pdf",
		"/home/admin/Jason/cec intern/results/[724]/Overall Wind Chart.pdf",
		#paste("/home/admin/Jason/cec intern/results/[724]/Wind Chart ",today,".pdf",sep=""),
		"/home/admin/Jason/cec intern/results/[IN-036S]/Rplots.pdf",
		"/home/admin/Jason/cec intern/results/[724]/Rplots.pdf")
		filestoSend=c(
		"KH-008S_Plots.pdf",
		"Cambodia_GIS.pdf",
		"Cebu_GIS.pdf",
		"Penang_GIS.pdf",
		"Ho_Chi_Minh_GIS.pdf",
		"Karnataka_GIS.pdf",
		"Delhi_GIS.pdf",
		"Singapore_GIS.pdf",
		"Chennai_GIS.pdf",
		"SG-006X_Plots.pdf",
		"724_Graphs.pdf",
		"725_Graphs.pdf",
		"Overall_Wind_Chart.pdf",
		#paste("Wind Chart ",today,".pdf",sep=""),
		"IN-036S.pdf",
		"724_Graphs(1).pdf")
		print("---------")
		print("KH-008Graph")
		source("/home/admin/CODE/EmailGraphs/KH008/Graphs.R")
		print("Penang")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Penang).R")
		print("Cebu")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Cebu).R")
		print("Cambodia")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Cambodia).R")
		print("Ho-Chi-Minh")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Ho Chi Minh City).R")
		print("Delhi")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Delhi).R")
		print("Chennai")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Chennai).R")
		print("Singapore")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Singapore).R")
		print("Karnataka")
		source("/home/admin/CODE/EmailGraphs/Task0/Pyro-to-Silicon Ratio - Code (Karnataka).R")
		print("Task2")
	#	source("/home/admin/CODE/EmailGraphs/Task2/Task2_SG-006X.R")
		print("Task4")
		source("/home/admin/CODE/EmailGraphs/Task4/Task_4_724_725.R")
		print("Task5")
		source("/home/admin/CODE/EmailGraphs/Task5/Task_5_724.R")
		print("Task6")
		source("/home/admin/CODE/EmailGraphs/Task6/IN036.R")
		print("---------")
		globCnt = 0
		tryCnt = 0
		while(tryCnt < 3)
		{
		currday = as.character(Sys.Date())
  	mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Graphs",substr(currday,1,10)),
            body = "PFA graphs for respecitve sites",
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathtoSend,
            file.names = filestoSend, # optional parameter
            debug = F),silent=T)
		if(class(mailSuccess) == "try-error")
		{
			print("Error sending mail")
			Sys.sleep(600)
			tryCnt = tryCnt + 1
			next
		}
		break
		}
	}
	print("Sleeping 1hr")
	Sys.sleep(3600)
	globCnt = globCnt + 1
	FORCESEND =0
}
sink()
