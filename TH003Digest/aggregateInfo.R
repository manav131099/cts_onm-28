source('/home/admin/CODE/common/aggregate.R')

registerMeterList("TH-003X",c("MDB-01 Check","MDB-01 Invoice","MDB-02 Check","MDB-02 Invoice"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 13 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "TH-003X" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-003X","MDB-01 Check",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 13 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "TH-003X" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-003X","MDB-01 Invoice",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 13 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "TH-003X" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-003X","MDB-02 Check",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 13 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "TH-003X" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-003X","MDB-02 Invoice",aggNameTemplate,aggColTemplate)
