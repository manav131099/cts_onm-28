source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-726S",c("Fab71","Fab72","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 3 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 11 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 9 #column for PR-1
aggColTemplate[10] = 10 #column for PR-2
aggColTemplate[11] = 8 #column for Irr
aggColTemplate[12] = "SG-724S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-726S","Fab71",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 3 #Column no for DA
aggColTemplate[3] = 20 #column for LastRead
aggColTemplate[4] = 21 #column for LastTime
aggColTemplate[5] = 14 #column for Eac-1
aggColTemplate[6] = 15 #column for Eac-2
aggColTemplate[7] = 16 #column for Yld-1
aggColTemplate[8] = 17 #column for Yld-2
aggColTemplate[9] = 18 #column for PR-1
aggColTemplate[10] = 19 #column for PR-2
aggColTemplate[11] = 8 #column for Irr
aggColTemplate[12] = "SG-724S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-726S","Fab72",aggNameTemplate,aggColTemplate)
