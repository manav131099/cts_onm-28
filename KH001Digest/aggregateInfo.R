source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-001S",c("M1","M2","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 18 #column for LastRead
aggColTemplate[4] = 20 #column for LastTime
aggColTemplate[5] = 5 #column for Eac-1
aggColTemplate[6] = 8 #column for Eac-2
aggColTemplate[7] = 16 #column for Yld-1
aggColTemplate[8] = 17 #column for Yld-2
aggColTemplate[9] = 14 #column for PR-1
aggColTemplate[10] = 15 #column for PR-2
aggColTemplate[11] = 13 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-001S","M1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 19 #column for LastRead
aggColTemplate[4] = 20 #column for LastTime
aggColTemplate[5] = 5 #column for Eac-1
aggColTemplate[6] = 8 #column for Eac-2
aggColTemplate[7] = 16 #column for Yld-1
aggColTemplate[8] = 17 #column for Yld-2
aggColTemplate[9] = 14 #column for PR-1
aggColTemplate[10] = 15 #column for PR-2
aggColTemplate[11] = 13 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-001S","M2",aggNameTemplate,aggColTemplate)

