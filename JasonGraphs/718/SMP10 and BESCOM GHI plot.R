rm(list=ls(all =TRUE))
library(ggplot2)
source('/home/admin/CODE/common/math.R')
i = "/home/admin/Jason/cec intern/results/718/[718]_da_summary.txt"
j = "/home/admin/Dropbox/GIS/Summary/Bescom/BESC Aggregate.txt"

temp1 <- read.table(i,header = T, sep = "\t")
temp2 <- read.table(j,header=T, sep = "\t")

temp1 <- temp1[,(-1)] #removing first column
#temp2 <- temp2[-length(temp2[,1]),]

temp3 <- as.data.frame(cbind(temp1,temp2))

gsiratio <- temp3[,7][temp3[,6]==100]/temp3[,13][temp3[,6]==100]

temp4 <- data.frame(Date =0,
                    SMP10 = temp3[,7][temp3[,6]==100],
                    GHI = temp3[,13][temp3[,6]==100])

temp4$Date = temp3[,1][temp3[,6]==100]
temp4$Month <- as.factor(substr(temp4$Date,3,7))
model <- lm(temp4[,3]~temp4[,2])

rsq <- format(as.numeric(summary(model)$r.squared),digits=3,nsmall = 3)

sd1 <- sdp(temp4[,2], na.rm = T)
sd2 <- sdp(temp4[,3], na.rm = T)

p1 <- ggplot(temp4, aes(x=SMP10,y=GHI,group=Month))
p1 <- p1 + geom_point(aes(shape = Month,colour = Month))
p1 <- p1 + scale_shape_manual(values=seq(0,(nlevels(temp4[,4])-1)))
p1 <- p1 + theme_bw()
p1 <- p1 + geom_abline(intercept = 0, slope = 1)
p1 <- p1 + expand_limits(x=0,y=0)
p1 <- p1 + coord_cartesian(ylim=c(0,8),xlim=c(0,8))
p1 <- p1 + xlab(expression(paste("SMP10 Global Horizontal Irradiation [",kwh/m^2,", daily]")))
p1 <- p1 + ylab(expression(paste("BESCOM Global Horizontal Irradiation [",kwh/m^2,", daily]")))
p1 <- p1 + ggtitle(paste("718 SPM10 vs BESCOM GIS"))
p1 <- p1 + annotate("text",label = paste0("R-sq error:",rsq),size=3.6, x = 4, y= 1)
p1 <- p1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.8,hjust = 0.5))

p1
ggsave(p1, filename = paste0("/home/admin/Jason/cec intern/results/718/gsi.pdf"),width = 7.92, height =5)

write.table(temp4,na = "", "/home/admin/Jason/cec intern/results/718/[718] SPM10 against BESCOM GIS.txt",row.names = FALSE,sep ="\t")
