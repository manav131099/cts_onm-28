rm(list=ls(all =TRUE))
library(ggplot2)

#working directory need to be changed
setwd("/Users/liuchenxi/Dropbox/Gen 1 Data/[IN-001]/2016/")
filelist <- dir(pattern = ".txt", recursive= TRUE)

#set the time interval for selecting data: 7AM to 5PM
timemin <- format(as.POSIXct("2016-09-17 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2016-09-17 17:00:00"), format="%H:%M:%S")
result <- NULL

#export data
for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  temp <- temp[condition,]
  date <- substr(paste(temp[1,1]),1,10)
  #select rows only when PAC is not NA
  temp <- temp[complete.cases(temp[,2]),]
  
  #index of PAC is different for different site. it is the 2nd column in site IN-001
  pac <- temp[,2]

  numer <- length(pac[pac>0.1])
  temp2 <- temp[pac<0.1,]
  temp2 <- temp2[temp2[,4]>20,]
  denom <- length(temp2[,1])+numer
  flagRate <- numer/denom
  data[1] <- date
  data[2] <- mean(pac)
  data[3] <- flagRate
  result <- rbind(result,data)
}

colnames(result) <- c("date","pac","frate")
rownames(result) <- NULL
result <- data.frame(result)
result <- result[-length(result[,1]),]
#write.table(result,file = "/Users/liuchenxi/Documents/CleanTech/result/IN-001/IN-001_PAC_LC.txt",
#            row.names = FALSE,sep ="\t")

result[,3] <- as.numeric(paste(result[,3]))*100
result[,1] <- as.Date(result[,1])
date = result[,1]

graph <- ggplot(result, aes(x=date,y=frate))+ylab("Uptime [%]")
final_graph<- graph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "1 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 100, 10))+
  coord_cartesian(ylim=c(0,100))+
  #change the title
  ggtitle(paste("Uptime for IN-001","\n",date[1]," to ",date[length(date)]))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.9,margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Uptime Percentage, Pac > 0.1 kW = ", format(round(mean(result[,3][!is.na(result[,3])]),1),nsmall=1),"%"),size=4,
           x = as.Date(date[round(0.518*length(date))]), y= 15)+
  annotate("text",label = paste0("System Lifetime = ", length(date)," days (",round(length(date)/365,1)," years)      "),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 10)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

final_graph

#ggsave(final_graph,filename = "/Users/liuchenxi/Documents/CleanTech/result/IN-001/IN-001_PAC_LC.pdf")

