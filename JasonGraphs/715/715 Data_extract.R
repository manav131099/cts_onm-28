rm(list=ls(all =TRUE))
library(ggplot2)

pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[715]"
pathwritetxt <- "/home/admin/Jason/cec intern/results/715/[715]_summary_PR_m1m2.txt"
pathwritecsv <- "/home/admin/Jason/cec intern/results/715/[715]_summary.csv"
nameofStation <- "715"

filelist <- dir(pathRead,pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2017-01-07 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-09-12 19:00:00"), format="%H:%M:%S")

stationLimit <- 1628.2

gsicol=3
paccol=71
eaccol=86
ins_capa=2592#2624
  
col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col8 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = col7[10^6] = col8[10^6] = 0
index <- 1

for (i in filelist[1:length(filelist)]){
  data <- NULL
  temp <- read.table(paste(pathRead,i,sep="/"),header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #gsi
  gsi <- as.numeric(temp[,3])
  col4[index] <- sum(gsi)/60000
  
  #eac method 1
  pac <-  abs(temp[,paccol])
  col5[index] <- sum(pac, na.rm = T)/60000
  
  #eac method 2
  col6[index] <- (temp[length(temp[,1]), eaccol] - temp[1, eaccol])/1000
  
  #pr via m1 and m2
  col7[index] <- (((sum(pac, na.rm = T)/60)/ins_capa/sum(gsi))*60000)*100
  col8[index] <- (((temp[length(temp[,1]), eaccol] - temp[1, eaccol])/ins_capa/sum(gsi))*60000)*100
  
  
  #ifelse(col7[index] >= 100 | col7[index] < 0, col7[index] = NA, NA)
  #ifelse(col8[index] >= 100 | col8[index] < 0, col8[index] = NA, NA)
  
  #ex_proc <- index/length(filelist)*100
  #print(paste(round(ex_proc,1), "%"))
  print(i)
  
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col7 <- col7[1:(index-1)]
col8 <- col8[1:(index-1)]

col7[col7>100 | col7<0] <- NA
col8[col8>100 | col8<0] <- NA


col2 <- col3/max(col3)*100

col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA
col8[is.na(col8)] <- NA

#col7 <- diff/stationLimit/sum(gsi)*60000
result <- cbind(col0,col1,col2,col3,col4,col5,col6,col7,col8)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI [W/m^2]","Eac method1","Eac  method2", "PR method1", "PR method2")

result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),2)
result[,6] <- round(as.numeric(result[,6]),2)
result[,7] <- round(as.numeric(result[,7]),2)
result[,8] <- round(as.numeric(result[,8]),2)
result[,9] <- round(as.numeric(result[,9]),2)

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
dataWrite <- result
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,pathwritecsv, na= "", row.names = FALSE)

###plot



