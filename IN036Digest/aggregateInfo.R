source('/home/admin/CODE/common/aggregate.R')

registerMeterList("IN-036S",c("CTR","STR","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 51 #Column no for DA
aggColTemplate[3] = 24 #column for LastRead
aggColTemplate[4] = 40 #column for LastTime
aggColTemplate[5] = 22 #column for Eac-1
aggColTemplate[6] = 47 #column for Eac-2
aggColTemplate[7] = 35 #column for Yld-1
aggColTemplate[8] = 48 #column for Yld-2
aggColTemplate[9] = 52 #column for PR-1
aggColTemplate[10] = 53 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 5 #column for Tamb
aggColTemplate[14] = 19 #column for Tmod
aggColTemplate[15] = 11 #column for Hamb

registerColumnList("IN-036S","CTR",aggNameTemplate,aggColTemplate)

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 51 #Column no for DA
aggColTemplate[3] = 21 #column for LastRead
aggColTemplate[4] = 39 #column for LastTime
aggColTemplate[5] = 21 #column for Eac-1
aggColTemplate[6] = 43 #column for Eac-2
aggColTemplate[7] = 34 #column for Yld-1
aggColTemplate[8] = 44 #column for Yld-2
aggColTemplate[9] = 54 #column for PR-1
aggColTemplate[10] = 55 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 5 #column for Tamb
aggColTemplate[14] = 19 #column for Tmod
aggColTemplate[15] = 11 #column for Hamb

registerColumnList("IN-036S","STR",aggNameTemplate,aggColTemplate)
