rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(858)
TOTSOLAR = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[KH-714S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[KH-714S] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,6])
			}
		}
	}
	return(c(gsiVal))
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			TIMESTAMPSALARM2 <<- NULL
			TIMESTAMPALARM3 <<- NULL
			TIMESTAMPALARM4 <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
		else if(METERCALLED ==4)
		{
			TIMESTAMPALARM4 <<- NULL
		}
	}
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread) == "try-error")
	{
		
		{
		date = Eac1 = DA = totrowsmissing = Yld1 = percSolar = Eac2 = Yld = RATIO = NA 
		lasttime = lastread = Yld2 = Gmod = Tamb =GPy=PRPy=PR1=PR2Py= ExpAmt=ExpPerc=NA
		if(METERCALLED == 1)
		{
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),
									DA = DA,Downtime = totrowsmissing,
									Yld1=Yld1,
									PercSolar = percSolar,
									#ExpAmt=expAmt,ExpPerc=expPerc,
									stringsAsFactors = F)
		
		}
		else if (METERCALLED == 2)
		{
			df = data.frame(Date = date, LoadEac = as.numeric(Eac1),LoadEac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,PercSolar=percSolar,Yld2=Yld2,
									#ExpAmt=expAmt,ExpPerc=expPerc,
									stringsAsFactors = F)
		}
		else if (METERCALLED == 3)
		{
				df = data.frame(Date = date, SolEac = as.numeric(Eac1),SolEac2= as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Gmod=Gmod,PR2=PR,Tamb=Tamb,Yld2=Yld2,
									GPy=GPy,PRPy=PRPy,PR1=PR1,PR2Py=PR2Py,GA=GA,PA=PA,
									stringsAsFactors = F)
		
		}
		}
		return()
	}
	dataread2 = dataread
	if(METERCALLED == 2)
		colnoUse = 37
	if(METERCALLED == 3)
		colnoUse = 45
	{
	if(METERCALLED == 2 || METERCALLED == 3)
	{
	idxpac = 18
	dataread = dataread2[complete.cases(as.numeric(dataread2[,colnoUse])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),colnoUse])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),colnoUse]) - as.numeric(dataread[1,colnoUse]))/1000,2),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
		if(METERCALLED == 2 || METERCALLED == 3)
			RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	}
	else
	{
	idxpac = 20
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = NA
	if(nrow(dataread) > 1)
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
	}
	}
	dataread = dataread2
	datareadcp = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,idxpac]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 4){
		 	 TIMESTAMPSALARM4 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
		indextouse = 1
	{
	  if(METERCALLED == 2 || METERCALLED == 3)
	  {
			Yld = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			Yld2 = format(round(as.numeric(Eac2)/INSTCAP[indextouse],2),nsmall=2)

			{
			if(METERCALLED == 2)
      {
			percSolar = round(TOTSOLAR*100/(TOTSOLAR+as.numeric(Eac2)),1)
			df = data.frame(Date = date, LoadEac = as.numeric(Eac1),LoadEac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,PercSolar=percSolar,Yld2=Yld2,stringsAsFactors = F)
			}
			else
			{
				print('Colnames is')
				dataread2 = datareadcp
				print(colnames(dataread2))
				dataTemp = as.numeric(dataread2[,(length(colnames(dataread2))-1)])
				dataTemp = dataTemp[complete.cases(dataTemp)]
				Gmod = Tamb = NA
				if(length(dataTemp))
					Gmod = round(sum(dataTemp)/60000,2)
				PR = round((as.numeric(Yld2) * 100 / Gmod),1)
				PR1 = round((as.numeric(Yld) * 100 / Gmod),1)
				dataTemp = as.numeric(dataread2[,(length(colnames(dataread2)))])
				dataTemp = dataTemp[complete.cases(dataTemp)]
				if(length(dataTemp))
					Tamb = round(mean(dataTemp),1)
				TOTSOLAR <<- as.numeric(Eac2)
				GPy = fetchGSIData(date)
				PRPy = round(as.numeric(Yld)*100/as.numeric(GPy),1)
				PRPy2 = round(as.numeric(Yld2)*100/as.numeric(GPy),1)
        timestamps_irradiance_greater_20 = dataread2[complete.cases(dataread2[, 63]>20), 1]	
        timestamps_freq_greater_40 = dataread2[complete.cases(dataread2[,2]>40), 1]	
        timestamps_pow_greater_2 = dataread2[complete.cases(dataread2[,18]>2), 1]	
        common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)	
        common2 = intersect(common, timestamps_pow_greater_2)	
        GA = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)	
        PA = round(((length(common2)/length(common))*100), 1)
				df = data.frame(Date = date, SolEac = as.numeric(Eac1),SolEac2= as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld=Yld,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Gmod=Gmod,PR2=PR,Tamb=Tamb,Yld2=Yld2,
									GPy=GPy,PR1=PR1,PRPy2=PRPy,GA=GA,PA=PA,
									stringsAsFactors = F)
			}
			}
    } 
	  else if(METERCALLED == 1)
	  {
			Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			percSolar = round(TOTSOLAR*100/(TOTSOLAR+as.numeric(Eac1)),1)
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),
									DA = DA,Downtime = totrowsmissing,
									Yld1=Yld1,
									PercSolar = percSolar, 
									stringsAsFactors = F)
		}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	
	dt = as.character(dataread2[,4])
	da = as.character(dataread2[,5])
	EacLoad1 = as.numeric(dataread1[,2])
	EacSol1Meth1 = as.numeric(dataread3[,2])
	EacSol1Meth2 = as.numeric(dataread3[,3])
	RatioSol1 = as.numeric(dataread3[,7])
	EacLoadMeth1 = as.numeric(dataread2[,2])
	EacLoadMeth2 = as.numeric(dataread2[,3])
	RatioLoad = as.numeric(dataread2[,7])
	LastReadSol = as.numeric(dataread3[,8])
	LastTimeSol = as.numeric(dataread3[,9])
	LastTimeLoad = as.numeric(dataread2[,8])
	LastReadLoad = as.numeric(dataread3[,9])
	GsiTot = as.numeric(dataread3[,10])
	GsiTot = as.numeric(dataread3[,10])
	PR = as.numeric(dataread3[,11])
	YldSol = as.numeric(dataread3[,6])
	YldLoad = as.numeric(dataread2[,6])
	YldLoad2 = as.numeric(dataread1[,5])
	Tamb = as.numeric(dataread3[,12])
	psolcomap = as.numeric(dataread1[,6])
	psolload = as.numeric(dataread2[,10])
	Gpy=as.numeric(dataread3[,14])
	PRPy=as.numeric(dataread3[,15])
	GA=as.numeric(dataread3[,17])	
  PA=as.numeric(dataread3[,18])
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacLoadCom = EacLoad1,
	YldLoadComp = YldLoad2,
	EacSolMeth1=EacSol1Meth1,
	EacSolMeth2=EacSol1Meth2,
	RatioSol=RatioSol1,
	LastTimeSol = LastTimeSol,
	LastReadSol = LastReadSol,
	GsiTot = GsiTot,
	PR = PR,
	YldSol = YldSol,
	EacLoadMeth1=EacLoadMeth1,
	EacLoadMeth2=EacLoadMeth2,
	RatioLoad=RatioLoad,
	LastTimeLoad = LastTimeLoad,
	LastReadLoad = LastReadLoad,
	YldLoad = YldLoad,
	Tamb = Tamb,
	PercSolarComAp =psolcomap, 
	PercSolarLoad = psolload,
	GPy=Gpy,
	PRPy=PRPy,
  GA=GA,	
  PA=PA,
	stringsAsFactors=F)

  {
    if(file.exists(writefilepath))
    {
			data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F)
			dates = as.character(data[,1])
			idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
			if(is.finite(idxmtch))
			{
				data = data[-idxmtch,]
				write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
			}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

